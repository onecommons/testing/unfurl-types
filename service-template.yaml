# Copyright (c) 2022 OneCommons Co.
# SPDX-License-Identifier: MIT
tosca_definitions_version: tosca_simple_unfurl_1_0_0 # unfurl/v1alpha1.ServiceTemplate
metadata:
  template_name: Generic cloud provider implementations
  template_author: onecommons.org
  template_version: 1.0.0

repositories:
  terraform-cloudinit-container-server:
    url: https://github.com/onecommons/terraform-cloudinit-container-server.git

imports:
  - repository: unfurl
    file: configurators/dns-template.yaml
  - repository: unfurl
    file: tosca_plugins/artifacts.yaml
  - repository: unfurl
    file: configurators/docker-template.yaml
  - file: aws/compute.yaml
  - file: gcp/compute.yaml
  - file: image-sources.yaml

relationship_types:
  unfurl.relationships.ConfiguredBy:
    derived_from: tosca.relationships.Root
    valid_target_types: [unfurl.capabilities.Configuration]

capability_types:
  unfurl.capabilities.Configuration:
    derived_from: tosca.capabilities.Root

node_types:
  unfurl.nodes.Configuration:
    derived_from: tosca.nodes.Root
    capabilities:
      configuredBy:
        type: unfurl.capabilities.Configuration
    requirements:
      - configures:
          relationship: unfurl.relationships.Configures
          occurrences: [0, 1] # optional

  unfurl.nodes.Service:
    derived_from: tosca:Root
    description: A service with one or more port and ip address endpoints
    properties:
      protocol:
        type: string
        default: https
      portspecs:
        description: Ports the service should be running on.
        type: list
        entry_schema:
          type: tosca.datatypes.network.PortSpec
    attributes:
      host:
        description: domain name or IP address of the endpoint
        type: string
        default: "{{ 'public_dns' | eval or 'public_address' | eval }}"
      port:
        type: tosca.datatypes.network.PortDef # this is just an integer
        default: # XXX better default: intersection of portspecs::0::source and host::ports
          eval: ports::0
    requirements:
      - host:
          occurrences: [0, 1]
          relationship: unfurl.relationships.ConfiguringHostedOn
          node: unfurl.nodes.ServiceHost

  unfurl.nodes.ServiceHost:
    derived_from: tosca:Root
    description: Addressable instance that hosts services
    attributes:
      private_address:
        type: string
        default:
          eval: .targets::host::private_address
      public_address:
        type: string
        default:
          eval: .targets::host::public_address
      public_dns:
        type: string
        required: false
        default:
          eval: .targets::host::public_dns
      ports:
        type: list
        entry_schema:
          type: tosca.datatypes.network.PortDef # this is just an integer
        default:
          eval: .configured_by::portspecs
          foreach: source

  unfurl.nodes.ContainerService:
    derived_from: unfurl.nodes.Service
    description: A service that runs a Docker (OCI) image or Docker Compose file
    properties:
      portspecs:
        type: list
        entry_schema:
          type: tosca.datatypes.network.PortSpec
        default:
          # map each string into a portspec
          eval: .::container::ports
          foreach:
            portspec:
              eval: $item
      files:
        type: list
        entry_schema:
          type: map
        default: []
      env:
        type: map
        entry_schema:
          type: string
        default: {}
      container_image:
        type: string
        default:
          if: .targets::container_image_source
          then:
            concat:
              - "{{ '.targets::container_image_source::registry_url' | eval }}"
              - "/"
              - "{{ '.targets::container_image_source::repository_id' | eval }}"
              - "{{ '.targets::container_image_source::repository_tag' | eval }}"
          else:
            eval: container::image
      container:
        type: unfurl.datatypes.DockerContainer
        required: false
    requirements:
      - host:
          relationship: unfurl.relationships.ConfiguringHostedOn
          node: unfurl.nodes.DockerHost
      - container_image_source:
          occurrences: [0, 1]
          node: ContainerImageSource

  GenericContainerService:
    derived_from: unfurl.nodes.ContainerService
    requirements:
      - host:
          # dockerhost is defined in deployment blueprint
          node: dockerhost
      - container_image_source:
          occurrences: [1, 1] # make required

  unfurl.nodes.App:
    derived_from: tosca:Root
    description: User-facing application composed of one or more services
    attributes:
      url:
        type: string

  unfurl.nodes.ContainerApp:
    derived_from: unfurl.nodes.App
    attributes:
      url:
        type: string
        default:
          {
            concat:
              [
                "{{ '.targets::container::protocol' | eval }}://",
                "{{ '.targets::container::host' | eval }}:",
                "{{ '.targets::container::port' | eval }}/",
              ],
          }
    requirements:
      - container:
          relationship: unfurl.relationships.Configures
          node: unfurl.nodes.ContainerService

  unfurl.nodes.WebApp:
    derived_from: unfurl.nodes.ContainerApp
    description: An app exposed through https
    properties:
      subdomain:
        type: string
        description: Choose a subdomain for your deployment. A subdomain of 'www', will be at www.your.domain
        default: www
        metadata:
          user_settable: true
        title: Subdomain
      admin_email:
        title: Admin Email
        type: string
        description: This email is registered with Lets Encrypt when generating TLS certificates.
        default: "{{ {'get_env': ['CI_PROJECT_PATH_SLUG', 'test']} | eval }}+letsencrypt-alerts@onecommons.org"
    attributes:
      url:
        type: string
        default: "https://{{ '.targets::container::host' | eval }}"

  unfurl.nodes.Compute:
    derived_from: unfurl.nodes.ServiceHost
    metadata:
      badge: Compute
    properties:
      num_cpus:
        title: CPUs
        type: integer
        constraints:
          - in_range: [1, 8]
        description: Number of CPUs for the server
      mem_size:
        title: Memory
        type: scalar-unit.size
        description: Amount of RAM (in MB)
        constraints:
          - in_range: [1gb, 20gb]
        metadata:
          default_unit: MB
      disk_size:
        title: Storage
        type: scalar-unit.size
        description: Size of boot disk (in GB)
        constraints:
          - in_range: [10gb, 2000gb]
        metadata:
          default_unit: GB
      boot_image:
        metadata:
          computed: true # we don't expect the user to set this
        type: string
      user_data:
        metadata:
          computed: true # we don't expect the user to set this
          visibility: hidden
          title: User Data
        type: string
      portspecs: # we need this to open up ports
        metadata:
          computed: true # we don't expect the user to set this
        type: list
        entry_schema:
          type: tosca.datatypes.network.PortSpec
      name:
        type: string
        default:
          # we need the application name to be unique
          # particularly because terraform will fail if it tries to create resources with names that already exist
          concat:
            - "{{ '.name' | eval  | truncate(25, True, (range(100000, 999999) | random | string)[1:]) }}"
            - "{{ lookup('env', 'DEPLOYMENT') | default('app', True) | truncate(25, True, (range(100000, 999999) | random | string)[1:]) }}"
            - "{{ '%M' | strftime }}"
          #concat: ["{{ '.name' | eval }}",  get_env: [DEPLOYMENT, "test-app"], "{{ '%M' | strftime }}"]
      # XXX zone, tags, network

  unfurl.nodes.BootImage:
    derived_from: unfurl.nodes.Configuration
    attributes:
      boot_image:
        type: string

  unfurl.nodes.DockerHost:
    derived_from: unfurl.nodes.ServiceHost
    properties:
      container:
        type: unfurl.datatypes.DockerContainer
        default:
          eval: .sources::host::container
      container_image:
        type: string
        default: "{{ '.sources::host::container_image' | eval }}"
      registry_url:
        type: string
        default: "{{ '.sources::host::.targets::container_image_source::registry_url' | eval }}"
        required: false
      registry_user:
        type: string
        default: "{{ '.sources::host::.targets::container_image_source::registry_user' | eval }}"
        required: false
      registry_password:
        type: string
        default: "{{ '.sources::host::.targets::container_image_source::registry_password' | eval }}"
        required: false

  unfurl.nodes.LocalDockerHost:
    derived_from:
      [unfurl.nodes.Container.Application.Docker, unfurl.nodes.DockerHost]
    description: Runs the container on your local Docker engine
    attributes:
      private_address:
        type: string
        default: "127.0.0.1"
      public_dns:
        type: string
        default: host.docker.internal

  # XXX
  # K8sContainerHost:
  #   description: run on Kubernetes using Kompose

  unfurl.nodes.DockerComputeHost:
    derived_from: unfurl.nodes.DockerHost
    description: Configure cloud init to run a Docker environment
    attributes:
      user_data:
        type: string
        metadata:
          title: User Data
    requirements:
      - host:
          relationship: unfurl.relationships.ConfiguringHostedOn
          metadata:
            title: Compute
          description: A compute instance with at least 1000MB RAM
          node: unfurl.nodes.Compute
          occurrences: [1, 1] # base class declares this as optional, override
          node_filter:
            # set properties on the compute instance to map to this node
            properties:
              - user_data:
                  eval: .configured_by::user_data
              - portspecs:
                  eval: .configured_by::portspecs
    interfaces:
      Standard:
        operations:
          configure:
            implementation: Terraform
            # set entry_state to initial so we can run this immediately
            # (the compute instance depends on user_data being created so we can't wait on it to be created)
            entry_state: initial
            outputs:
              cloud_config:
            inputs:
              resultTemplate:
                readyState: ok # we have to explicitly set this because terraform doesn't report any resources as changed
                attributes:
                  user_data: "{{ outputs.cloud_config }}"
              tfvars:
                files: "{{ '.configured_by::files' | eval }}"
                env: "{{ '.configured_by::env' | eval }}"
                container: "{{ SELF.container }}"
                image: "{{ SELF.container_image }}"
                registry_url: "{{ SELF.registry_url }}"
                registry_user: "{{ SELF.registry_user }}"
                registry_password: "{{ SELF.registry_password }}"
              main: docker-compute-host

  unfurl.nodes.TraefikDockerComputeHost:
    derived_from: unfurl.nodes.DockerComputeHost
    metadata:
      title: Traefik Docker Compute Host
    description: Configure cloud init to run a Docker environment fronted by Trafik serving HTTPS
    properties:
      letsencrypt_staging:
        type: boolean
        default:
          eval: .targets::dns::testing
    attributes:
      ports:
        type: list
        entry_schema:
          type: tosca.datatypes.network.PortDef # this is just an integer
        default:
          - 443
      public_dns:
        type: string
        description: Fully qualified domain name.
        default: "{{ '.configured_by::subdomain' | eval }}.{{ '.targets::dns::name' | eval }}"
    interfaces:
      Standard:
        operations:
          configure:
            implementation: Terraform
            # set entry_state to initial so we can run this immediately
            # (the compute instance depends on user_data being created so we can't wait on it to be created)
            entry_state: initial
            outputs:
              cloud_config:
            inputs:
              resultTemplate:
                readyState: ok # we have to explicitly set this because terraform doesn't report any resources as changed
                attributes:
                  user_data: "{{ outputs.cloud_config }}"
              tfvars:
                email: "{{ '.configured_by::admin_email' | eval }}"
                domain: "{{ SELF.public_dns }}"
                files: "{{ '.configured_by::files' | eval }}"
                env: "{{ '.configured_by::env' | eval }}"
                letsencrypt_staging: "{{ SELF.letsencrypt_staging }}"
                container: "{{ SELF.container }}"
                image: "{{ SELF.container_image }}"
                registry_url: "{{ SELF.registry_url }}"
                registry_user: "{{ SELF.registry_user }}"
                registry_password: "{{ SELF.registry_password }}"
              main:
                eval:
                  get_dir: terraform-cloudinit-container-server
    requirements:
      - dns:
          metadata:
            title: DNS
          node: unfurl.nodes.DNSZone
          description: "DNS provider for domain name configuration"
          relationship:
            type: unfurl.relationships.DNSRecords
            properties:
              records:
                "{{ '.source::.configured_by::subdomain' | eval }}":
                  type: A
                  value:
                    eval: .source::.targets::host::public_address

  Route53DNSZone:
    derived_from: unfurl.nodes.DNSZone
    description: Amazon Web Services Route53 DNS provider for domain name configuration
    metadata:
      badge: DNS
      title: Amazon Web Services DNS
      icon: /onecommons/unfurl-types/-/raw/main/icons/aws-route53.svg
    properties:
      console_url:
        type: string
        default: https://console.aws.amazon.com/route53/v2/hostedzones
      access_key_id:
        type: string
        title: Access Key ID
        default:
          get_env: AWS_ACCESS_KEY_ID
        metadata:
          user_settable: true
      secret_access_key:
        title: Secret Access Key
        type: string
        default:
          get_env: AWS_SECRET_ACCESS_KEY
        metadata:
          sensitive: true
          user_settable: true
      session_token:
        title: AWS session token
        type: string
        default:
          get_env: AWS_SESSION_TOKEN
        metadata:
          sensitive: true
      provider:
        type: map
        metadata:
          computed: true
        default:
          class: octodns.provider.route53.Route53Provider
          access_key_id:
            eval: access_key_id
          secret_access_key:
            eval: secret_access_key
          session_token:
            eval: session_token

  DigitalOceanDNSZone:
    derived_from: unfurl.nodes.DNSZone
    description: Digitial Ocean DNS provider for domain name configuration
    metadata:
      badge: DNS
      title: DigitalOcean DNS
      icon: /onecommons/unfurl-types/-/raw/main/icons/digital_ocean.svg
    properties:
      console_url:
        type: string
        default: https://cloud.digitalocean.com/networking/domains
      DIGITALOCEAN_TOKEN:
        title: DigitalOcean Personal Access Token
        type: unfurl.datatypes.EnvVar
        description: An API token for your DigitalOcean account.
        default:
          get_env: DIGITALOCEAN_TOKEN
        metadata:
          sensitive: true
          user_settable: true
      provider:
        type: map
        metadata:
          computed: true
        default:
          class: octodns.provider.digitalocean.DigitalOceanProvider
          token:
            eval: DIGITALOCEAN_TOKEN

  GoogleCloudDNSZone:
    derived_from: unfurl.nodes.DNSZone
    description: Google Cloud DNS provider for domain name configuration
    metadata:
      badge: DNS
      title: Google Cloud DNS
      icon: /onecommons/unfurl-types/-/raw/main/icons/google-cloud-dns.png
    properties:
      console_url:
        type: string
        default: https://console.cloud.google.com/net-services/dns/zones?project={{ SELF.provider.project }}
      provider:
        type: map
        metadata:
          computed: true
        default:
          class: octodns.provider.googlecloud.GoogleCloudProvider
          credentials_file:
            get_env: GOOGLE_APPLICATION_CREDENTIALS
          project:
            get_env: CLOUDSDK_CORE_PROJECT
    interfaces:
      defaults:
        implementation:
          className: unfurl.configurators.dns.DNSConfigurator
          dependencies:
            - google-cloud-dns

  AzureDNSZone:
    derived_from: unfurl.nodes.DNSZone
    description: Azure DNS provider for domain name configuration
    metadata:
      badge: DNS
      title: Azure DNS
      icon: /onecommons/unfurl-types/-/raw/main/icons/azure.svg
    properties:
      client_id:
        type: string
        title: Client Id
        description: The Azure Active Directory Application ID (aka client ID)
        default:
          get_env: AZURE_CLIENT_ID
      secret_key:
        type: string
        description: Authentication Key Value (secret)
        metadata:
          sensitive: true
        default:
          get_env: AZURE_SECRET
      directory_id:
        type: string
        title: Directory ID
        description: Directory ID (aka Tenant ID)
        default:
          get_env: AZURE_TENANT
      subscription_id:
        type: string
        title: Subscription ID
        default:
          get_env: AZURE_SUBSCRIPTION_ID
      resource_group:
        type: string
        title: Resource Group
        required: false
      provider:
        type: map
        metadata:
          computed: true
        default:
          class: octodns.provider.azure.AzureProvider
          client_id:
            eval: client_id
          directory_id:
            eval: directory_id
          key:
            eval: secret_key
          sub_id:
            eval: subscription_id
          resource_group:
            eval: resource_group

  unfurl.nodes.MongoDBWebApp:
    derived_from: unfurl.nodes.WebApp
    description: A web app that uses MongoDB
    requirements:
      - db:
          relationship: unfurl.relationships.Configures
          node: MongoDB
          metadata:
            title: Database
          description: MongoDB database service

  unfurl.nodes.SoftwareService:
    derived_from: tosca:Root
    properties:
      software_version:
        type: string
        metadata:
          title: Software Version
        required: false

  MongoDB:
    derived_from: unfurl.nodes.SoftwareService
    metadata:
      badge: Database
    properties:
      software_version:
        title: "MongDB Version"
        type: string
        default: "11.2"

  AtlasService:
    derived_from: [MongoDB, unfurl.nodes.Installer.Terraform]
    metadata:
      title: MongoDB Atlas Service
    properties:
      public_key:
        type: string
        title: Public Key
        description: "Public API key to authenticate Atlas"
      private_key:
        type: string
        title: Private Key
        description: "Private API key to authenticate Atlas"
        metadata:
          sensitive: true
    interfaces:
      defaults:
        inputs:
          main:
            eval:
              abspath: [AtlasService, src]
          tfvars:
            public_key:
              eval: public_key
            private_key:
              eval: private_key
            mongodbversion:
              eval: software_version
        outputs:
          username:
          ip_address:
          connection_string:
      Mock:
        operations:
          configure:
            implementation: Template
            inputs:
              dryrun: true
              resultTemplate:
                attributes:
                  username: admin
                  ip_address: 127.0.0.1
                  connection_string: mongodb:foo

  MongoDBInstance:
    derived_from: [MongoDB, unfurl.nodes.ContainerService]
    metadata:
      title: Self-hosted MongoDB
      badge: Database
    properties:
      container:
        type: unfurl.datatypes.DockerContainer
        default:
          image: bitnami/mongodb
          ports:
            - 27017:27017
          # XXX environment
    requirements:
      - host:
          node: db-dockerhost

  SMTPServer:
    derived_from: tosca:Root
    description: SMTP Mail server for sending mail
    metadata:
      title: Mail Server
      badge: Mail
    properties:
      host:
        title: "SMTP host"
        type: string
      port:
        title: SMTP Port
        type: tosca.datatypes.network.PortDef
        default: 587
      user_name:
        title: "User Name" # or email
        type: string
      authentication:
        title: "Authentication"
        type: string
        constraints:
          - valid_values: [plain]
        default: "plain"
      password:
        title: Secret
        type: string
        metadata:
          sensitive: true
      protocol:
        type: string
        valid_values: ["ssl", "tls"]
        required: false
      starttls_auto:
        title: "Automatically Start TLS"
        type: boolean
        default: true
    interfaces:
      Install:
        connect: echo "connected"

  GenericSMTPServer:
    derived_from: SMTPServer
    metadata:
      title: Generic SMTP Server
    description: SMTP mail server for sending mail.

  SendGrid:
    derived_from: SMTPServer
    description: Use sendgrid.com to send mail.
    metadata:
      icon: /onecommons/unfurl-types/-/raw/main/icons/Sendgrid.svg
    properties:
      password:
        type: string
        title: API Key
        metadata:
          sensitive: true
      host:
        title: "SMTP host"
        type: string
        default: smtp.sendgrid.net
      user_name:
        type: string
        default: apikey
      protocol:
        type: string
        default: tls

  unfurl.nodes.SQLWebApp:
    derived_from: unfurl.nodes.WebApp
    description: A web app that uses a SQL database like MySQL or Postgres
    requirements:
      - db:
          relationship: unfurl.relationships.Configures
          node: SqlDB
          metadata:
            title: Database

  SqlDB:
    derived_from: unfurl.nodes.SoftwareService
    metadata:
      badge: Database
    properties:
      root_user:
        type: string
        default: root
        metadata:
          user_settable: true
      root_password:
        type: string
        default:
          _generate: { "preset": "password" }
        metadata:
          sensitive: true
          user_settable: true
      database_name:
        description: Name of initial database to create (optional)
        type: string
        default: null
        required: false
      database_type:
        type: string
        default: null
        metadata:
          user_settable: false

  MySQLDB:
    derived_from: SqlDB
    metadata:
      badge: Database

  MariaDBInstance:
    derived_from: [MySQLDB, unfurl.nodes.ContainerService]
    description: https://github.com/bitnami/bitnami-docker-mariadb
    metadata:
      title: Self-hosted MariaDB
      badge: Database
      icon: /onecommons/unfurl-types/-/raw/main/icons/MariaDBInstance.svg
    properties:
      database_type:
        type: string
        default: MARIADB
      container:
        type: unfurl.datatypes.DockerContainer
        default:
          user: "0:0"
          image: bitnami/mariadb:latest
          volumes:
            - /var/app/mariadb:/bitnami/mariadb
          ports:
            - 3306:3306
          environment:
            MARIADB_DATABASE:
              eval: database_name
            MARIADB_ROOT_USER:
              eval: root_user
            MARIADB_ROOT_PASSWORD:
              eval: root_password
            # XXX MARIADB_PORT_NUMBER
            # set properties on the compute instance to map to this node
    requirements:
      - host:
          node: db-dockerhost

  GoogleSQLStorage:
    derived_from: [unfurl.nodes.Installer.Terraform, unfurl.nodes.Service]
    metadata:
      title: GoogleCloud SQLStorage
      icon: /onecommons/unfurl-types/-/raw/main/icons/google-cloud-sql.svg
      badge: Database
    properties:
      instance_name:
        type: string
        default:
          eval: .name
      database_type:
        type: string
        valid_values: [MYSQL, POSTGRES, SQLSERVER]
      instance_type:
        type: string
        default: db-n1-standard-2
    requirements:
      - host:
          occurrences: [0, 0]
    interfaces:
      defaults:
        implementation: Terraform
        inputs:
          main: gcp/sqlstorage
      Standard:
        requirements:
          - unfurl.relationships.ConnectsTo.GoogleCloudProject
        operations:
          delete:
          create:
            implementation: Terraform
            inputs:
              tfvars:
                instancename: "{{ SELF.instance_name }}"
                dbversion: "{{SELF.database_type}}_{{ SELF.software_version | replace('.', '_')}}"
                dbname: "{{ SELF.database_name }}"
                dbuser: "{{ SELF.root_user }}"
                dbpass: "{{ SELF.root_password }}"
                instancetier: "{{ SELF.instance_type }}"
                deletion_protection: true # TODO pass this option to the user
                project_id:
                  get_env: CLOUDSDK_CORE_PROJECT
                zone:
                  get_env: CLOUDSDK_COMPUTE_ZONE
              resultTemplate:
                readyState: ok
                protected: true
                attributes:
                  host: "{{ outputs.host }}"
                  console_url:
                    eval:
                      python: gcp/helpers.py#get_gcpsql_console_url
                      args:
                        name: "{{ outputs.connection_name }}"
            outputs:
              host:
              connection_name:
      Mock:
        operations:
          delete:
          create:
            inputs:
              resultTemplate:
                attributes:
                  console_url:
                    eval:
                      python: gcp/helpers.py#get_gcpsql_console_url
                      args:
                        name: unfurl-353419:us-central1:terraform-20220617203722584300000001

  GoogleSQLStorageMySQL:
    derived_from: [MySQLDB, GoogleSQLStorage]
    metadata:
      title: Google Cloud SQL for MySQL
      icon: /onecommons/unfurl-types/-/raw/main/icons/google-cloud-sql.svg
      badge: Database
    properties:
      database_type:
        type: string
        default: MYSQL
      software_version:
        type: string
        default: "8.0"
      portspecs:
        type: list
        entry_schema:
          type: tosca.datatypes.network.PortSpec
        default:
          - eval:
              portspec: 3306:3306

  AWSRDS:
    derived_from: [unfurl.nodes.Installer.Terraform, unfurl.nodes.Service]
    metadata:
      title: Fully-Managed DBs by AWS
      badge: Database
      icon: /onecommons/unfurl-types/-/raw/main/icons/aws-rds.svg
    properties:
      instance_class:
        type: string
        default: "db.t3.micro"
      engine:
        type: string
        description: See https://docs.aws.amazon.com/AmazonRDS/latest/APIReference/API_CreateDBInstance.html for valid values.
        valid_values: [postgres, mariadb, mysql]
      database_type:
        type: string
        valid_values: [MYSQL, POSTGRES, MARIADB, SQLSERVER]
    requirements:
      - host:
          occurrences: [0, 0]
    interfaces:
      defaults:
        implementation: Terraform
        inputs:
          main: aws/rds
      Standard:
        requirements:
          - unfurl.relationships.ConnectsTo.AWSAccount
        operations:
          delete:
          create:
            inputs:
              tfvars:
                instance_class: "{{ SELF.instance_class }}"
                engine: "{{ SELF.engine }}"
                engine_version: "{{ SELF.software_version }}"
                dbuser: "{{ SELF.root_user }}"
                dbpass: "{{ SELF.root_password }}"
                dbname: "{{ SELF.database_name }}"
                port: "{{ SELF.portspecs[0].source }}"
                deletion_protection: true # TODO pass this option to the user
              resultTemplate:
                readyState: ok
                protected: true
                attributes:
                  host: "{{ outputs.host }}"
                  console_url:
                    eval:
                      python: aws/helpers.py#get_awsrds_console_url
                      args:
                        arn: "{{ outputs.rds_arn }}"
            outputs:
              host:
              rds_arn:
      Mock:
        operations:
          delete:
          create:
            inputs:
              resultTemplate:
                attributes:
                  console_url:
                    eval:
                      python: aws/helpers.py#get_awsrds_console_url
                      args:
                        name: arn:aws:rds:us-west-2:362020207168:db:dbinstance

  AwsRdsMySQL:
    derived_from: [MySQLDB, AWSRDS]
    metadata:
      title: AWS RDS managed MySQL
      badge: Database
      icon: /onecommons/unfurl-types/-/raw/main/icons/aws-rds.svg
    properties:
      engine:
        type: string
        default: mysql
      software_version:
        type: string
        default: "5.1"
      database_type:
        type: string
        default: MYSQL
      portspecs:
        type: list
        entry_schema:
          type: tosca.datatypes.network.PortSpec
        default:
          - eval:
              portspec: 3306:3306

  AwsRdsPostgres:
    derived_from: [PostgresDB, AWSRDS]
    metadata:
      title: AWS RDS managed PostgresSQL
      badge: Database
      icon: /onecommons/unfurl-types/-/raw/main/icons/aws-rds.svg
    properties:
      engine:
        type: string
        default: postgres
      software_version:
        type: string
        default: "13.4"
      database_type:
        type: string
        default: POSTGRES
      portspecs:
        type: list
        entry_schema:
          type: tosca.datatypes.network.PortSpec
        default:
          - eval:
              portspec: 5432:5432

  AwsRdsMariaDB:
    derived_from: [MySQLDB, AWSRDS]
    metadata:
      title: AWS RDS managed MariaDB
      badge: Database
      icon: /onecommons/unfurl-types/-/raw/main/icons/aws-rds.svg
    properties:
      engine:
        type: string
        default: mariadb
      software_version:
        type: string
        default: "10.6.7"
      database_type:
        type: string
        default: MARIADB
      portspecs:
        type: list
        entry_schema:
          type: tosca.datatypes.network.PortSpec
        default:
          - eval:
              portspec: 3306:3306

  PostgresDB:
    derived_from: SqlDB
    metadata:
      badge: Database
    properties:
      database_type:
        type: string
        default: POSTGRES

  GoogleSQLStoragePostgres:
    derived_from: [PostgresDB, GoogleSQLStorage]
    metadata:
      title: Google Cloud SQL managed PostgresSQL
      icon: /onecommons/unfurl-types/-/raw/main/icons/google-cloud-sql.svg
      badge: Database
    properties:
      database_type:
        type: string
        default: POSTGRES
      software_version:
        type: string
        default: "12"
      instance_type:
        type: string
        default: db-custom-1-3840
      portspecs:
        type: list
        entry_schema:
          type: tosca.datatypes.network.PortSpec
        default:
          - eval:
              portspec: 5432:5432

  PostgresDBInstance:
    derived_from: [PostgresDB, unfurl.nodes.ContainerService]
    description: https://github.com/bitnami/bitnami-docker-postgresql
    metadata:
      title: Self-hosted PostgresDB
      badge: Database
      icon: /onecommons/unfurl-types/-/raw/main/icons/PostgresDB.svg
    properties:
      container:
        type: unfurl.datatypes.DockerContainer
        default:
          user: "0:0"
          image: bitnami/postgresql:latest
          volumes:
            - /var/app/postgresql:/bitnami/postgresql
          ports:
            - 5432:5432
          environment:
            POSTGRESQL_USERNAME:
              eval: root_user
            POSTGRESQL_DATABASE:
              eval: database_name
            POSTGRESQL_PASSWORD:
              eval: root_password
            # XXX
            # POSTGRESQL_PORT_NUMBER:
            # eval: .sources::container::postgresql_port_number
    requirements:
      - host:
          node: db-dockerhost

  Redis:
    derived_from: unfurl.nodes.SoftwareService
    metadata:
      badge: Database
    properties:
      port:
        title: Redis Port
        type: tosca.datatypes.network.PortDef
        constraints:
          - in_range: [1024, 491511]
        default: 6379
        description: Port number to listen on.
      password:
        type: string
        required: false
        constraints:
          - pattern: "^[^@]+$"
          # The at sign (@) is not supported for REDIS_PASSWORD.
        default:
          _generate: { "preset": "password" }
        metadata:
          sensitive: true
          user_settable: true

  RedisInstance:
    derived_from: [Redis, unfurl.nodes.ContainerService]
    description: https://github.com/bitnami/bitnami-docker-redis
    metadata:
      title: Self-hosted Redis
      badge: Database
      icon: /onecommons/unfurl-types/-/raw/main/icons/redis-icon.svg
    properties:
      container:
        type: unfurl.datatypes.DockerContainer
        default:
          image: bitnami/redis:latest
          ports:
            - "{{ SELF.port }}:{{ SELF.port }}"
          environment:
            REDIS_PASSWORD:
              eval: password
            REDIS_PORT_NUMBER:
              eval: port
    requirements:
      - host:
          node: redis-dockerhost

topology_template:
  inputs:
    app_name:
      type: string
      default:
        get_env: [DEPLOYMENT, "test-app{{ '%M' | strftime }}"]

  node_templates:
    dockerhost:
      type: unfurl.nodes.DockerHost
      description: This is an incomplete, placeholder template that needs to be defined in your blueprint
      directives:
        - default

    db-dockerhost:
      type: unfurl.nodes.DockerHost
      description: This is an incomplete, placeholder template that needs to be defined in your blueprint
      directives:
        - default

    redis-dockerhost:
      type: unfurl.nodes.DockerHost
      description: This is an incomplete, placeholder template that needs to be defined in your blueprint
      directives:
        - default
