locals {
  container = {
    for k, v in var.container : k => v if !contains(["image", "container_name", "command"], k)
  }

  # Merge the container definition provided by the user with the default template.
  # The resulting object should match the schema expected by Docker Compose.

  docker_compose_template_yaml = file("${path.module}/docker-compose-template.yaml")
  docker_compose_template      = yamldecode(local.docker_compose_template_yaml)

  docker_compose = {
    version = "3.3"
    services = {
      app = merge(local.docker_compose_template.services.app, local.container, {
        for key, val in local.docker_compose_template.services.app : key =>
        can(tolist(val)) && contains(keys(local.container), key)
        ? try(setunion(val, lookup(local.container, key, [])), val)
        : lookup(local.container, key, val)
      })
    }
    networks = {
      default = {
        external = {
          name = "$${DOCKER_NETWORK}"
        }
      }
    }
  }

  docker_compose_yaml = yamlencode(local.docker_compose)

  login = var.registry_user ? "-u ${var.registry_user} -p ${var.registry_password} ${var.registry_url}" : null

}

locals {
  environment = merge({
    COMPOSE_DOCKER_IMAGE = var.docker_compose_image
    COMPOSE_DOCKER_TAG   = var.docker_compose_tag
    IMAGE_NAME           = try(split(":", var.image)[0], null)
    IMAGE_TAG            = try(split(":", var.image)[1], "latest")
    CONTAINER_NAME       = lookup(var.container, "container_name", null)
    CONTAINER_COMMAND    = lookup(var.container, "command", null)
    CONTAINER_PORT       = lookup(var.container, "port", null)
    DOCKER_NETWORK       = "web"
    DOCKER_LOG_DRIVER    = null
  }, var.env)

  file_regex = "(?P<filename>docker-compose(?:\\.(?P<name>.*?))?\\.ya?ml)"
  files = concat(
    [
      {
        filename = ".env"
        content  = base64encode(join("\n", [for k, v in local.environment : "${k}=${v}" if v != null]))
      }
    ],
    var.files,
    var.image ?
    [{ filename = "docker-compose.yaml", content = base64encode(local.docker_compose_yaml) }]
    : []
  )

  # From the list above, identify all docker-compose*.yaml files.
  # This list will be used to generate separate systemd unit files for each service.
  docker_compose_files = [
    for f in local.files : merge(regex(local.file_regex, f.filename), f)
    if can(regex(local.file_regex, f.filename))
  ]
}
// Generate cloud-init config
data "cloudinit_config" "config" {
  gzip          = false
  base64_encode = false
  part {
    filename     = "cloud-init.yaml"
    merge_type   = "list(append)+dict(no_replace,recurse_list)+str()"
    content_type = "text/cloud-config"
    content = templatefile("${path.module}/cloud-config.yaml", {
      files                = local.files
      docker_compose_files = local.docker_compose_files
      login                = local.login
    })
  }
  # Add any additional cloud-init configuration or scripts provided by the user
  dynamic "part" {
    for_each = var.cloudinit_part
    content {
      merge_type   = "list(append)+dict(no_replace,recurse_list)+str()"
      content_type = part.value.content_type
      content      = part.value.content
    }
  }
}
output "cloud_config" {
  description = "Content of the cloud-init config to be deployed to a server."
  value       = data.cloudinit_config.config.rendered
}
variable "env" {
  description = "List of environment variables (KEY=VAL) to be made available within the application container and also Docker Compose (useful for overriding configuration options)."
  type        = map(string)
  default     = {}
}
variable "files" {
  description = "User files to be copied to the application's working directory (`/var/app`). The file's content must be provided to Terraform as a base64 encoded string."
  type        = list(object({ filename : string, content : string }))
  default     = []
}
variable "cloudinit_part" {
  description = "Additional cloud-init configuration used to setup and/or customise the instance beyond the defaults provided by this module."
  type        = list(object({ content_type : string, content : string }))
  default     = []
}
variable "docker_compose_image" {
  description = "Docker image used to run Docker Compose commands. (default: docker/compose)"
  type        = string
  default     = "docker/compose"
}
variable "docker_compose_tag" {
  description = "Tagged version of Docker Compose to use. (default: latest)"
  type        = string
  default     = "latest"
}
variable "container" {
  description = "Object containing the definition of the container to deploy. The key and values from this object are interpolated directly into the Docker Compose file used to run your application, refer to the Docker Compose documentation for more information."
  type        = any
  default     = {}
}
variable "image" {
  type    = string
  default = null
}
variable "registry_url" {
  type    = string
  default = null
}

variable "registry_user" {
  type    = string
  default = null
}
variable "registry_password" {
  type      = string
  default   = null
  sensitive = true
}



