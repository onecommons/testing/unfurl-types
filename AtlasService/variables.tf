variable "public_key"{
    type = string
    description = "Public Programmatic API key to authenticate Atlas"
}

variable "private_key"{
    type = string
    description = "Private Programmatic API key to authenticate Atlas"
    sensitive = true
}   

variable "org_id"{
    type = string
    description = "MongoDB Organization ID"
}

variable "project_name"{
    type = string
    description = "MongoDB Atlas Project Name"
}

variable "project_owner_id"{
    type = string
    description = "MongoDB Atlas Project Owner ID"
}

variable "cluster_name"{
    type = string
    description = "MongoDB Atlas Cluster Name"
}

variable "cloud_provider" {
    type = string
    description = "MongoDB Atlas Cluster Name"
}

variable "region" {
    type = string
    description = "MongoDB Atlas Cluster Region"
}

variable "mongodbversion" {
    type = string
    description = "MongoDB Atlas Version"
}

variable "dbuser" {
    type = string
    description = "MongoDB Atlas Database User Name"
}

variable "dbuser_password" {
    type = string
    description = "MongoDB Atlas Database User Password"
}

variable "database_name" {
    type = string
    description = "Database Name"
}
