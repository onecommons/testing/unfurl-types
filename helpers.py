import logging
import os
import json
from toscaparser.elements.scalarunit import ScalarUnit_Size

log = logging.getLogger(__file__)


def choose_machine_type(ctx, args):
    """Choose machine type based on memory and cpu"""
    num_cpus = args["num_cpus"]
    mem_size = args["mem_size"]
    mem_size = ScalarUnit_Size(mem_size).get_num_from_scalar_unit("MiB")

    types = args["machine_types"]
    if not os.getenv("UNFURL_MOCK_DEPLOY"):
        types = filter(lambda x: x["mem"] >= mem_size and x["cpu"] >= num_cpus, types)
    types = sorted(types, key=lambda x: (x["cpu"], x["mem"]))

    if types:
        log.info(
            "Selected machine type: %s [CPU: %s, Memory: %s MiB]",
            types[0]["name"],
            types[0]["cpu"],
            types[0]["mem"],
        )
        return types[0]["name"]
    raise ValueError(
        "Can't find satisfactory machine type ({} cpus, {} mem).".format(
            num_cpus, mem_size
        )
    )

def create_variable(ctx, args):
    ctx.task.logger.debug("create variable %s %s", type(args.get('value')), args.get('value'))
    try:
        return _create_variable(args.pop('key'), args.pop('value'), **args)
    except:
        if ctx.task:
            ctx.task.logger.error("creating variable failed", exc_info=True)
        return None

def _create_variable(key, value, variable_type="env_var", masked=False, protected=False):
    CI_SERVER_URL = os.getenv('CI_SERVER_URL')
    ACCESS_TOKEN = os.getenv('UNFURL_ACCESS_TOKEN')
    CI_PROJECT_ID = os.getenv("CI_PROJECT_ID")
    ENVIRONMENT = os.getenv('CI_ENVIRONMENT_NAME', "*")
    if not (CI_SERVER_URL and ACCESS_TOKEN and CI_PROJECT_ID):
        return None

    if not isinstance(value, str):
        value = json.dumps(value)

    import gitlab

    gitlab = gitlab.Gitlab(CI_SERVER_URL, private_token=ACCESS_TOKEN)
    project = gitlab.projects.get(CI_PROJECT_ID)
    # https://python-gitlab.readthedocs.io/en/stable/api/gitlab.v4.html#gitlab.v4.objects.ProjectVariableManager
    return project.variables.create(
              dict(
                environment_scope=ENVIRONMENT,
                key=key,
                protected=protected,
                masked=masked,
                value=str(value),
                variable_type=variable_type
              ))

