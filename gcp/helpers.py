import os
import re
from unfurl.configurator import Configurator


def get_compute_console_url(ctx, args):
    """Get GCP compute console URL for a given resource ID."""
    resource_id_regex = r"^projects/([A-z]|[0-9]|-)*/zones/([A-z]|[0-9]|-)*/instances/([A-z]|[0-9]|-)*$"
    resource_id_pattern = re.compile(resource_id_regex)
    gcp_console_format = "https://console.cloud.google.com/compute/instancesDetail/zones/{zone}/instances/{name}?project={project}"

    resource_id = args["resource_id"]

    if not resource_id_pattern.match(resource_id):
        raise ValueError(
            f"Invalid resource ID, resource ID must match {resource_id_regex}")

    # id format: projects/{{project}}/zones/{{zone}}/instances/{{name}}
    split = resource_id.split("/")

    parsed = {
        "project": split[1],
        "zone": split[3],
        "name": split[5]
    }

    return gcp_console_format.format(**parsed)


def get_gcpsql_console_url(ctx, args):
    """Get GCP SQL console URL for a given connection name."""
    connection_name_regex = r"^([A-z]|[0-9]|-)*:([A-z]|[0-9]|-)*:([A-z]|[0-9]|-)*$"
    connection_name_pattern = re.compile(connection_name_regex)
    gcp_console_format = "https://console.cloud.google.com/sql/instances/{name}/overview?project={project}"

    connection_name = args["name"]

    if not connection_name_pattern.match(connection_name):
        raise ValueError(
            f"Invalid resource ID, resource ID must match {connection_name_pattern}")

    # id format: {{project}}:{{region}}:{{instance}}
    split = connection_name.split(":")

    parsed = {
        "project": split[0],
        "zone": split[1],
        "name": split[2]
    }
    return gcp_console_format.format(**parsed)


def get_clouddns_console_url(ctx, args):
    pass

class MetadataConfigurator(Configurator):
    def run(self, task):
        task.logger.info("Fetching machine types")
        task.target.attributes["machine_types"] = list(
            self.all_machine_types(task))
        yield task.done(True)

    def can_dry_run(self, task):
        return True

    @staticmethod
    def all_machine_types(task):
        # delay imports until now so the python package can be installed first
        from google.cloud.compute_v1 import ListMachineTypesRequest, MachineTypesClient

        request = ListMachineTypesRequest()
        project = os.getenv("CLOUDSDK_CORE_PROJECT")
        if not project:
            raise ValueError(
                "Can't choose machine type - CLOUDSDK_CORE_PROJECT not defined"
            )
        zone = os.getenv("CLOUDSDK_COMPUTE_ZONE")
        if not zone:
            raise ValueError(
                "Can't choose machine type - CLOUDSDK_COMPUTE_ZONE not defined"
            )
        request.project = project
        request.zone = zone

        try:
            client = MachineTypesClient()
            response = client.list(request)
        except Exception as e:
            task.logger.error("GCP: %s", e)
            raise ValueError(
                "Can't find machine types. Can't communicate with GCP.")

        yield from (
            {
                "name": item.name,
                "mem": item.memory_mb,
                "cpu": item.guest_cpus,
            }
            for item in response.items
        )


if __name__ == "__main__":
    print(get_compute_console_url(
        None, {"resource_id": "projects/test/zones/test/instances/test"}))
