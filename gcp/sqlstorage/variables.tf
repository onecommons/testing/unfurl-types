
variable "instancename" {
    type = string
    description = "Unique name of the database server instance"
}

variable "dbversion" {
    type = string
    description = "This defines which database is being used"
}

variable "instancetier" {
    type = string
    description = "The instance tier"
}

variable "dbname" {
    type = string
    description = "This defines the name of the databse"
}

variable "dbuser" {
    type = string
    description = "This defines the database user name"
}

variable "dbpass" {
    type = string
    description = "This defines the database password"
}

variable "project_id" {
    type = string
}

variable "zone" {
    type = string
}

variable "deletion_protection" {
    type = bool
    default = true
}
