terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 4.19"
    }

    google-beta = {
      source  = "hashicorp/google-beta"
      version = "~> 4.19"
    }
  }
}

module "enable_google_cloud_sql" {
  source  = "terraform-google-modules/project-factory/google//modules/project_services"
  version = "13.0"

  project_id                  = var.project_id
  disable_services_on_destroy = false

  activate_apis = [
    "sqladmin.googleapis.com",
    "iam.googleapis.com",
  ]
}

// Wait for google cloud sql api to be ready, before creating the instance
// This is needed because google apis are not ready immediately after being marked
// as enabled because of eventual consistency.
resource "time_sleep" "wait_30_seconds_for_google_apis" {
  depends_on = [module.enable_google_cloud_sql]

  create_duration = "30s"
}



/* network service */
/* peering */
/* https://github.com/terraform-google-modules/terraform-google-sql-db/tree/master/modules/private_service_access */
/* https://github.com/terraform-google-modules/terraform-google-sql-db */
/* https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/service_networking_connection */

resource "google_sql_database_instance" "default"{
    # Ensure for sqladmin.googleapis.com is enabled
    depends_on = [
      resource.time_sleep.wait_30_seconds_for_google_apis,
    ]

    database_version = var.dbversion
    deletion_protection = var.deletion_protection
    settings {
        tier = var.instancetier
        ip_configuration {
            /* private_network = "projects/${var.project_id}/global/networks/default" */
            ipv4_enabled = true
            authorized_networks {
                name = "internet"
                value = "0.0.0.0/0"
            }
        }
        location_preference {
            zone = var.zone
        }
    }
}

/* must use global/networks */
/* │ Error: "settings.0.ip_configuration.0.private_network" ("projects/oc-staging1/us-central1-a/networks/default") doesn't match regexp "projects/((?:(?:[-a-z0-9]{1,63}\\.)*(?:[a-z](?:[-a-z0-9]{0,61}[a-z0-9])?):)?(?:[0-9]{1,19}|(?:[a-z0-9](?:[-a-z0-9]{0,61}[a-z0-9])?)))/global/networks/((?:[a-z](?:[-a-z0-9]*[a-z0-9])?))$" */



resource "google_sql_database" "db" {
    count = "1"
    name = var.dbname
    instance = google_sql_database_instance.default.name
    charset = "utf8"
  
}

resource "google_sql_user" "user" {
    name= var.dbuser
    instance = google_sql_database_instance.default.name
    password = var.dbpass
}
