output "rds_hostname" {
  description = "RDS instance hostname"
  value       = aws_db_instance.rds_instance.address  
}

output "host" {
  description = "RDS instance hostname"
  value       = aws_db_instance.rds_instance.address  
}

output "rds_port" {
  description = "RDS instance port"
  value       = aws_db_instance.rds_instance.port
}

output "rds_username" {
  description = "RDS instance root username"
  value       = aws_db_instance.rds_instance.username
}

output "rds_arn" {
  description = "RDS Amazon Resource Name"
  value = aws_db_instance.rds_instance.arn
}
