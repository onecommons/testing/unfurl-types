resource "aws_security_group" "sg_rds" {
  ingress {
    from_port   = var.port
    to_port     = var.port
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = var.port
    to_port     = var.port
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "RDS_SG"
  }
}

resource "aws_db_instance" "rds_instance" {
  instance_class         = var.instance_class
  allocated_storage      = 5
  engine                 = var.engine
  /* engine_version         = "${var.engine_version}-R1" */
  username               = var.dbuser
  password               = var.dbpass
  db_name                = var.dbname
  vpc_security_group_ids = [aws_security_group.sg_rds.id]
  publicly_accessible    = true
  skip_final_snapshot    = true
}
